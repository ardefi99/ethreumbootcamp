1. npm install -g truffle (already installed !), $ mkdir this_dir , $ cd this_dir , this_dir$ truffle unbox react --- already excuted!!! 
- truffle.com -> react 
- https://github.com/truffle-box/react-box -> truffle-box.json
```
{
  "ignore": ["README.md", ".gitignore", "box-img-lg.png", "box-img-sm.png", ".github"],
  "commands": {
    "Compile": "truffle compile",
    "Migrate": "truffle migrate",
    "Test contracts": "truffle test",
    "Test dapp": "cd client && npm test",
    "Run dev server": "cd client && npm run start",
    "Build for production": "cd client && npm run build"
  },
  "hooks": {
    "post-unpack": "cd client && npm ci"
  }
}
```
//change to client dir, then install all the dependencis in /client/package.json - "dependencies" { ... }
2. vscode : change solidity compiler : crtl+shift+p -> change workspace compiler version(Remote) : 0.6.0
3. node --version -> 14(Now installed version, which causes error)
- nvm : node version manager
- $ wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
-> already installed !!

* //Node 12 works with truffle version 5.0.x and 5.1.x

```
$ command -v nvm => shows "nvm"-> success nvm install
- $ nvm ls-remote
- $ nvm ls
- $ nvm install node //will install the latest version of Node.js
- $ nvm install 12.19.0  //<SPECIFIC_NODE_VERSION>

$ nvm use node 
$ nvm use 12.19.0
//need to specific versin locally installed, if not, default version excuted.

//nvm run node or nvm run --version like nvm run 11.10.0 will switch to the specified ////version of Node.js and open up a Node command line for you to run commands manually ////from afterwards.

- run node
$ nvm run node //default
$ nvm run 12.19.0 //like nvm run 11.10.0

<!-- nvm alias default node, or the specified version with nvm alias default --version like nvm alias default 11.10.0.
Once this default alias is set, any new shell will default to running with that version of Node. -->
$ nvm alias default node
$ nvm alias default 11.10.0
------------------------

```

- in command line in vscode   
```
> nvm use 12.19.0
> truffle development 


```
